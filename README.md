# Cellars.IO SDK

## Installing the library from the private repository

bower install cellars.js=git@bitbucket.org:cellier/cellars-io-sdk.git#~0.5.0 --save

## Getting Started

Include the library in your index.html

    <script src="js/cellars.js"></script>


Instantiate a new engine: 

    var platform = new Cellars();
    
By default, the engine will point to https://api.cellars.io. If you want to work with a local development engine, just use the url option: 

    var platform = new Cellars({url: 'http://localhost:5000'});
    
Cellars.IO is using JWT tokens for authentication. In order to use most of the available methods and helpers, you need to retrieve a valid token. 

### Configuring our SDK to retrieve a token

You need to set the following options in order to retrieve a valid token:

    url: Url of the cellars.io instance you want to use. default to https://api.cellars.io
    auth: {
        token: An existing token (previously retrieved) or nothing 
    },
    unsafe: true indicates that we're in an unsafe environment (client app, browser, etc)
    app_key : The key of the app as provided by cellars.io
    app_secret: Optional. The secret if you are in a safe environment (do not use for client or browser app) 

With these options you can instantiate the Cellars instance: 

    var platform = new Cellars({
        unsafe: isClient,
        app_key: "b4423a98460c03213d7ad86033622f5066a44b69"
    });

Note that we haven't provided any token as we don't have one yet. 

### Retrieving a valid token

You execute the authenticate method to retrieve your token : 

    platform.authenticate({username:username, password:password}).then(function(result) {
    
        // Save the token for subsequent calls
        platform.auth = {
            token: result.token
        };
        
    });


### Verify a token

You can save tokens between sessions. When reloading them, just use the verifyToken method to know if this token can still
be used to access the platform. 

    platform.verifyToken(savedToken).then(function(resp) {
    
        if(resp.valid) {
            // Can use the token
            platform.auth.token = token;
            return resp.token;
        }
        else {
            // Must retrieve a new token (using username/password)
        }
            
    });

## Available Methods

* **authenticate**: Retrieve a secure token (and session information) to perform other secure calls. 
* **getCurrentUser**: Retrieve information about the currently authenticated user
* **getSystemStatus**: Retrieve the current system status. Perfect way to detect if the platform is up or not. 
* **products**: show, create, update remove, upsert products
* **producers**: show, create, update remove, upsert producers
* **raw**: Methods to perform batch manipulation of raw product data. 
* **social**: 
* **inventory**: Use to manage user inventory
* **workers**:
* **winelists**: 
* **search**:
* **users**:

## Stream API

Cellars.IO provides a powerful streaming API that enables the creation of real-time web and mobile applications. Streams are channels 
where events are broadcast by the platform to reflect changes. 

The following channels are available in the SDK

* **catalog**: Receives all events related to the improvement and modification in the product catalog
* **marketplace**: Receive events related to marketplace activities. 
* **cellar**: Receives events related to cellar activities. 
* **social**: Receives events related to social activities. 
* **profile**: Receives events related to user profile activities.
* **system**: Receives events related to system activities. 

### Subscribing to a channel

You can subscribe to a channel by using the subscribe method of the channel you which to subscribe: 

```js 
cellar.cellar.subscribe({
    token: "my-secure-token",
    handler: function(event) {
        this === myScope;
    },
    scope: myScope,
    type: ['improve', 'create']
});
```

The previous example will execute the handler callback, with ```myScope``` as function scope each time an improve or create event is 
produced by the catalog. 

### Disabling Stream API

You can disable stream API and avoid establishing a web socket connection to the platform by using the no_stream options when instantiating
cellars. 

```js
var cellars = new Cellars({
    no_stream: true 
});
```

You can learn more about the stream API in the SDK [WIKI](https://bitbucket.org/cellier/cellars-io-sdk/wiki/Stream%20API%20Guide).
