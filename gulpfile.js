var gulp = require('gulp');
var gutil = require('gulp-util');
var bump = require('gulp-bump');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');
var testem = require('gulp-testem');
var mocha = require('gulp-mocha');
var jsdoc = require("gulp-jsdoc");

var bundler = watchify(browserify('./src/index.js', {
    standalone: 'Cellars',
    cache: {},
    packageCache: {},
    fullPaths: true
}));

/**
 * Bumping version number and tagging the repository with it.
 * Please read http://semver.org/
 *
 * You can use the commands
 *
 *     gulp patch     # makes v0.1.0 → v0.1.1
 *     gulp feature   # makes v0.1.1 → v0.2.0
 *     gulp release   # makes v0.2.1 → v1.0.0
 *
 * To bump the version numbers accordingly after you did a patch,
 * introduced a feature or made a backwards-incompatible release.
 */

function incrementVersion(importance) {
    // get all the files to bump version in
    return gulp.src(['./package.json', './bower.json'])
        // bump the version number in those files
        .pipe(bump({ type: importance }))
        // save it back to filesystem
        .pipe(gulp.dest('./'));
}

// add any other browserify options or transforms here
bundler.transform('brfs');

gulp.task('default', bundle); // so you can run `gulp js` to build the file
bundler.on('update', bundle); // on any dep update, runs the bundler
bundler.on('log', gutil.log); // output build logs to terminal

function bundle() {
    incrementVersion('patch');

    return bundler.bundle()
        // log errors if they happen
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('cellars.js'))
        // optional, remove if you dont want sourcemaps
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        .pipe(sourcemaps.write('./')) // writes .map file
        //
        .pipe(gulp.dest('./dist'));
}

gulp.task('test', function() {
    return gulp.src('test/**/*-spec.js', {read: false})
        .pipe(mocha({reporter: 'nyan'}));
});

/**
 * Test distribution file in a browser using testem
 */
gulp.task('e2e-test', function () {

    bundle();

    gulp.watch('./src/**/*.js', bundle);
    gulp.watch('./test/**/*.js', bundle);

    gulp.src([''])
        .pipe(testem({
            configFile: 'testem.json'
        }));
});

gulp.task('doc', function() {
    return gulp.src("./src/**/*.js")
        .pipe(jsdoc('./dist/apidoc'));
});
