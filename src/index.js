/**
 * Cellars.IO SDK
 */
var HttpClient = require('./lib/http-client');
var events = require('events');
var inherits = require('inherits');
var StreamAPI = require('./lib/stream_api');
var _ = require('lodash');
var Logger = require('./lib/logger');

function Cellars(options) {
    options = options || {};
    this.base_url = options.url || 'https://api.cellars.io';
    this.auth = options.auth || {};
    events.EventEmitter.call(this);
    this.logger = new Logger(options.log_level || 6);
    this.api_version = options.api_version || '1.0';
    this.quality_level = options.quality_level || 8;
    this.http = new HttpClient(this.base_url, {app_key: options.app_key, unsafe: options.unsafe, logger: this.logger, api_version: this.api_version, quality_level: this.quality_level });

    if(!options.no_stream) {
        this.streamAPI = new StreamAPI(_.merge({url: this.base_url, logger: this.logger}, options.streamAPI || {}));
    }
    else {
        this.logger.info('Disabling stream API as requested by caller');
    }

    this.read_store = options.read_store || options.store;
    this.write_store = options.write_store || options.store;
    this.force_remote = options.force_remote || {};

    // Install all supported API endpoints
    this.marketvalue = require('./lib/methods/marketvalue')({platform: this});
    this.aspects = require('./lib/methods/aspects')({platform: this});
    this.producers = require('./lib/methods/producers')({platform: this});
    this.raw = require('./lib/methods/raw')({platform: this});
    this.sellingPrice = require('./lib/methods/sellingPrice')({platform: this});
    this.search = require('./lib/methods/search')({platform: this});
    this.workers = require('./lib/methods/workers')({platform: this});
    this.winelists = require('./lib/methods/winelists')({platform: this});
    this.winecards = require('./lib/methods/winecards')({platform: this});
    this.users = require('./lib/methods/users')({platform: this});
    this.social = require('./lib/methods/social')({platform: this});
    this.inventory = require('./lib/methods/inventory')({platform: this});
    this.crm = require('./lib/methods/crm')({platform: this});
    this.security = require('./lib/methods/security')({platform: this});
    this.community = require('./lib/methods/community')({platform:this});
    this.products = require('./lib/methods/products')({platform: this});
    this.cms = require('./lib/methods/cms')({platform: this});
    this.catalog = require('./lib/methods/catalog')({platform: this});
    this.marketplace = require('./lib/methods/marketplace')({platform: this});
    this.stores = require('./lib/methods/stores')({platform: this});
    this.custom = require('./lib/methods/custom')({platform: this});
    this.analysis = require('./lib/methods/analysis')({platform: this});
    this.corporation = require('./lib/methods/corporation')({platform: this});

    /** @obsolete */
    this.social_events = require('./lib/methods/social_events')({platform: this});

}

inherits(Cellars, events.EventEmitter);

// Register all available methods
Cellars.prototype.getCurrentUser = require('./lib/methods/get-current-user');
Cellars.prototype.getSystemStatus = require('./lib/methods/get-system-status');
Cellars.prototype.authenticate = require('./lib/methods/authenticate');
Cellars.prototype.verifyToken = require('./lib/methods/verify_token');
Cellars.prototype.social_connect = require('./lib/methods/social_connect');

Cellars.prototype.useHttpClient = function(httpClient) {
    this.logger.debug('Overriding platform HTTP client');
    var opts = {
        url : this.http.url,
        app_key: this.http.app_key,
        app_secret: this.http.app_secret,
        unsafe: this.http.unsafe
    };
    
    httpClient.setOptions(opts);
    this.http = httpClient;
    this.logger.info('Platform HTTP client has been overriden');
};

module.exports = Cellars;
