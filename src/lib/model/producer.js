var BaseModel = require('../base_model');
var inherits = require('inherits');

function Model(options) {
    BaseModel.call(this, 'producers', options);
}
inherits(Model, BaseModel);

module.exports = Model;
