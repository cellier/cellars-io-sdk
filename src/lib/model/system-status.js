var _ = require('lodash');

function SystemStatus() {
}

SystemStatus.prototype.print = function(prompt) {
    this.ready(function(result) {
        console.log('Response received!');
        console.log(prompt, result);
    });
    return this;
};

SystemStatus.prototype.check = function(ok, fail) {
    return this.ready(function(result) {
        if(result.statusCode === 200) {
            if(_.isFunction(ok)) {
                ok.call(this, result);
            }
            else {
                console.log(ok);
            }
        }
        else {
            if(_.isFunction(fail)) {
                fail.call(this, result);
            }
            else {
                console.log(fail);
            }
        }
    });
};

module.exports = SystemStatus;
