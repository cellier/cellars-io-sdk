var BaseModel = require('../base_model');
var inherits = require('inherits');

function Model(options) {
    options = options || {};
    options.prefix = '/inventory';
    BaseModel.call(this, 'presets', options);
}
inherits(Model, BaseModel);

module.exports = Model;
