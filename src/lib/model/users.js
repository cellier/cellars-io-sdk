var BaseModel = require('../base_model');
var inherits = require('inherits');

function Model(options) {
    BaseModel.call(this, 'users', options);
}
inherits(Model, BaseModel);

Model.prototype.updateProfile = function(username, profile) {
    return this.platform.http.put('/users/'+username, profile, {token: this.platform.auth.token });
};

module.exports = Model;
