var _ = require('lodash');

function Aspects(options) {
    options = options || {};
    this.platform = options.platform;
}

/**
 *
 * @param keywords
 * @param options
 * @returns {*}
 */
Aspects.prototype.ApplyBestMarketValue = function(AUPID, options) {
    options = options || {};
    return this.platform.http.put('/catalog/'+ AUPID + '/marketvalue/best', {}, { token: this.platform.auth.token });
};

/**
 *
 * @param keywords
 * @param options
 * @returns {*}
 */
Aspects.prototype.AddRetailer = function(AUPID, retailer) {
    return this.platform.http.post('/catalog/'+ AUPID + '/retailer', {'retailer':retailer}, { token: this.platform.auth.token });
};

Aspects.prototype.RemoveRetailerCode = function(AUPID, aspectSourceKey, productSourceKey, code) {
    return this.platform.http.delete('/catalog/'+ AUPID + '/retailer/' + aspectSourceKey +  '/' + productSourceKey +'/' + code, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Aspects(options);
};
