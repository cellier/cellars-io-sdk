'use strict';
function SellingPrice(options) {
    options = options || {};
    this.platform = options.platform;
}

SellingPrice.prototype.listRules = function () {
    return this.platform.http.get('/winecard/sellingPrice/rules', { token: this.platform.auth.token });
};

SellingPrice.prototype.updateRules = function (sellingPriceRules) {

    return this.platform.http.put('/winecard/sellingPrice/rule',
        sellingPriceRules, { token: this.platform.auth.token });
};

SellingPrice.prototype.search = function (query) {

    return this.platform.http.get('/winecard/sellingPrice/search',
        { query: query, token: this.platform.auth.token });
};

SellingPrice.prototype.update = function (aupid, data) {

    return this.platform.http.put('/winecard/sellingPrice/' + aupid,
        data, { token: this.platform.auth.token });
};

SellingPrice.prototype.applyRules = function () {

    return this.platform.http.post('/winecard/sellingPrice/calculate', {},
        { token: this.platform.auth.token });
};

module.exports = function (options) {
    options = options || {};
    return new SellingPrice(options);
};

