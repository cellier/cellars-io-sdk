var _ = require('lodash');

function Stores(options) {
    options = options || {};
    this.platform = options.platform;
}

/**
 *
 * @param keywords
 * @param options
 * @returns {*}
 */
Stores.prototype.getStoreAvailability = function(AUPID, options) {
    options = options || {};
    return this.platform.http.get('/products/'+ AUPID + '/store-availability', {}, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Stores(options);
};
