var Users = require('../model/users');

module.exports = function(options) {
    options = options || {};
    return new Users(options);
};
