
module.exports = (function() {

    return function(options) {
        options = options || {};

        return this.http.get('/user', {
            token: options.token || this.auth.token
        });

    };

})();
