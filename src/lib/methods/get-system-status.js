var SystemStatus = require('../model/system-status');

module.exports = (function() {

    return function(options) {
        return this.http.get('/status', options || {}).mixin(SystemStatus);
    };

})();
