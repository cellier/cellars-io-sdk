var _ = require('lodash');

function Community(options) {
    options = options || {};
    this.platform = options.platform;
}

Community.prototype.create = function(type, data) {
    switch(type) {
    case 'event':
        return this.platform.http.post('/events', data, {token: this.platform.auth.token });
    case 'productReview':
        return this.platform.http.post('/products/'+data.AUPID+'/reviews', data, {token: this.platform.auth.token });
    }
};

Community.prototype.update = function(type, key, data) {
    if(arguments.length === 2) {
        data = key;
        key = data.key;
    }

    switch(type) {
    case 'event':
        return this.platform.http.put('/events/'+key, data, {token: this.platform.auth.token });
    case 'productReview':
        return this.platform.http.put('/products/'+data.AUPID+'/reviews/'+key, data, {token: this.platform.auth.token });
    }
};

Community.prototype.list = function(type, data, options) {
    this.platform.logger.debug('Listing %s', type);
    switch(type) {
    case 'event':
        return this.platform.http.get('/events', {token: this.platform.auth.token });
    case 'event-types':
        return this.platform.http.get('/event-types', {token: this.platform.auth.token });
    case 'productReview':
        return this.platform.http.get('/products/'+data+'/reviews', {token: this.platform.auth.token, query: options });
    }
};

Community.prototype.search = function(type, query, options) {
    options = options || {};
    switch(type) {
    case 'event':
        return this.platform.http.post('/search/events', query, _.merge(options, {token: this.platform.auth.token }));
    case 'productReview':
        return this.platform.http.post('/search/product_reviews', query, _.merge(options, {token: this.platform.auth.token }));
    }
};

Community.prototype.load = function(type, key, data, options) {
    this.platform.logger.debug('Loading %s %s', type, key, data);

    switch(type) {
    case 'event':
        return this.platform.http.get('/events/'+key, {query: data, token: this.platform.auth.token });
    case 'productReview':
        return this.platform.http.get('/products/'+data+'/reviews/'+key, {token: this.platform.auth.token, query: options });
    }
};

Community.prototype.remove = function(type, key, data) {
    this.platform.logger.debug('Removing %s %s', type, key, data);
    switch(type) {
    case 'event':
        return this.platform.http.delete('/events/'+key, {token: this.platform.auth.token });
    case 'productReview':
        return this.platform.http.delete('/product_reviews/'+key, {token: this.platform.auth.token });
    }
};

Community.prototype.follow = function(username, relationships) {
    return this.platform.http.post('/community/'+username+'/relationships', relationships, {token: this.platform.auth.token });
};

Community.prototype.unfollow = function(username, targetUsername) {
    return this.platform.http.delete('/community/'+username+'/relationships/'+targetUsername, {token: this.platform.auth.token });
};

Community.prototype.rateProductReview = function(AUPID, reviewKey, rating)  {
    return this.platform.http.post('/products/'+AUPID+'/reviews/'+reviewKey+'/ratings', { rating: rating}, {token: this.platform.auth.token });
};

Community.prototype.searchProductsInEvent = function(eventKey, identity, options) {
    return this.platform.http.post('/search/events/'+eventKey+'/products', {
        identity: identity,
        options: options
    }, {token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Community(options);
};
