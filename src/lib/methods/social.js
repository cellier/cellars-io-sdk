var _ = require('lodash');

function Social(options) {
    options = options || {};
    this.platform = options.platform;
}

Social.prototype.connect = function(provider, data, options) {
    options = options || {};
    return this.platform.post('/social/'+provider, { body: data}, options);
};

module.exports = function(options) {
    options = options || {};
    return new Social(options);
};
