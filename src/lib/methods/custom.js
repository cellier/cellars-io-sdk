
function Custom(options) {
    options = options || {};
    this.platform = options.platform;
}

Custom.prototype.get = function(path, options) {
    options = options || {};
    options.token = options.token || this.platform.auth.token;
    return this.platform.http.get(path, options);
};

Custom.prototype.post = function(path, data, options) {
    options = options || {};
    options.token = options.token || this.platform.auth.token;
    return this.platform.http.post(path, data, options);
};

Custom.prototype.put = function(path, data, options) {
    options = options || {};
    options.token = options.token || this.platform.auth.token;
    return this.platform.http.put(path, data, options);
};

Custom.prototype.delete = function(path, options) {
    options = options || {};
    options.token = options.token || this.platform.auth.token;
    return this.platform.http.delete(path, options || {});
};

module.exports = function(options) {
    options = options || {};
    return new Custom(options);
};
