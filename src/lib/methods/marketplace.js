var _ = require('lodash');

function Marketplace(options) {
    options = options || {};
    this.platform = options.platform;
}

Marketplace.prototype.sync = function(type, data, options) {
    options = options || {};
    return this.platform.http.post('/marketplace/stores', data, _.assign(options, {token: this.platform.auth.token}));
};

Marketplace.prototype.listFavoriteStores  = function(options) {
    options = options || {};
    return this.platform.http.get('/users/me/favorite_stores', { query: options, token: this.platform.auth.token });
};

Marketplace.prototype.listNearbyStores  = function(query) {
    return this.platform.http.get('/marketplace/nearby_stores', { query: query, token: this.platform.auth.token });
};

Marketplace.prototype.searchStores = function(query, options) {
    options = options || {};
    return this.platform.http.post('/search/marketplace/stores', query, _.merge(options, {token: this.platform.auth.token }));
};

Marketplace.prototype.toggleFavoriteStore = function(storeKey) {
    return this.platform.http.put('/users/me/favorite_stores/'+storeKey, {}, {token: this.platform.auth.token });
};

Marketplace.prototype.searchProducts = function(query) {
    return this.platform.http.post('/search/marketplace/products', query,  {token: this.platform.auth.token });
};

Marketplace.prototype.pushBottle = function(acode, qualityImage) {
    return this.platform.http.post('/marketplace/push/'+acode, {qualityImage: qualityImage},  {token: this.platform.auth.token });
};

Marketplace.prototype.popBottle = function(acode) {
    return this.platform.http.delete('/marketplace/'+acode,{},  {token: this.platform.auth.token });
};

Marketplace.prototype.soldBottle = function(acode, data) {
    return this.platform.http.put('/marketplace/sold/'+acode, data,  {token: this.platform.auth.token });
};

Marketplace.prototype.listBottle = function(data) {
    return this.platform.http.get('/marketplace/list', data,  {token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Marketplace(options);
};
