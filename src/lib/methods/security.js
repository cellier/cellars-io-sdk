'use strict';
var _ = require('lodash');

function Security(options) {
    options = options || {};
    this.platform = options.platform;
}

Security.prototype.createUser = function(spec) {
    return this.platform.http.post('/users', spec, { token: this.platform.auth.token});
};

Security.prototype.createApplication = function(spec) {
    return this.platform.http.post('/applications', spec, { token: this.platform.auth.token});
};

Security.prototype.createGroup = function(spec) {
    return this.platform.http.post('/groups', spec, { token: this.platform.auth.token});
};

Security.prototype.createToken = function(spec) {
    return this.platform.http.post('/tokens', spec, { token: this.platform.auth.token});
};

module.exports = function(options) {
    options = options || {};
    return new Security(options);
};
