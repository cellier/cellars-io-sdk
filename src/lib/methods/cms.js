'use strict';
var _ = require('lodash');

function Cms(options) {
    options = options || {};
    this.platform = options.platform;
}

Cms.prototype.create = function(key, spec, options) {
    options = options || {};
    spec.key = key;
    return this.platform.http.post('/documents', spec, _.merge(options, { token: this.platform.auth.token }));
};

Cms.prototype.update = function(key, spec, options) {
    options = options | {};
    return this.platform.http.put('/documents/'+key, spec, _.merge(options, { token: this.platform.auth.token }));
};

Cms.prototype.show = function(key, options) {
    options = options | {};
    return this.platform.http.get('/documents/'+key, { query: options, token: this.platform.auth.token });
};

Cms.prototype.list = function(query) {
    return this.platform.http.get('/documents', { query: query, token: this.platform.auth.token });
};

Cms.prototype.search = function(query) {
    return this.platform.http.post('/search/documents', query, { token: this.platform.auth.token });
};

Cms.prototype.remove = function(key) {
    return this.platform.http.delete('/documents/'+key, _.merge(options, { token: this.platform.auth.token }));
};

module.exports = function(options) {
    options = options || {};
    return new Cms(options);
};
