'use strict';

function Winelists(options) {
    options = options || {};
    this.platform = options.platform;
}

Winelists.prototype.create = function (winelistSpec) {
    return this.platform.http.post('/winelists', winelistSpec, { token: this.platform.auth.token });
};

Winelists.prototype.update = function (winelistSpec) {
    return this.platform.http.put('/winelists/' + winelistSpec.key, winelistSpec, { token: this.platform.auth.token });
};

Winelists.prototype.render = function (winelistKey, options) {
    return this.platform.http.get('/winelists/' + winelistKey + '/products', { query: options || {}, token: this.platform.auth.token });
};

Winelists.prototype.load = function (winelistKey) {
    return this.platform.http.get('/winelists/' + winelistKey, { token: this.platform.auth.token });
};

Winelists.prototype.remove = function (winelistKey) {
    return this.platform.http.delete('/winelists/' + winelistKey, { token: this.platform.auth.token });
};

Winelists.prototype.list = function () {
    return this.platform.http.get('/winelists', { token: this.platform.auth.token });
};

Winelists.prototype.searchProducts = function (key, spec) {
    return this.platform.http.post('/winelists/' + key + '/products', spec, { token: this.platform.auth.token });
};

Winelists.prototype.addToList = function (key, products) {
    return this.platform.http.put('/winelists/' + key + '/products', products, { token: this.platform.auth.token });
};

Winelists.prototype.removeFromList = function (key, AUPID) {
    return this.platform.http.delete('/winelists/' + key + '/products/' + AUPID, { token: this.platform.auth.token });
};

Winelists.prototype.toggleProduct = function (key, AUPID) {
    return this.platform.http.put('/winelists/' + key + '/products/' + AUPID, { token: this.platform.auth.token });
};

Winelists.prototype.rename = function (key, title) {
    return this.platform.http.put('/winelists/' + key + '/rename', {title:title}, { token: this.platform.auth.token });
};

module.exports = function (options) {
    options = options || {};
    return new Winelists(options);
};
