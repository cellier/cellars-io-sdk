var Producer = require('../model/producer');

module.exports = function(options) {
    options = options || {};
    return new Producer(options);
};
