var _ = require('lodash');

function Raw(options) {
    options = options || {};
    this.platform = options.platform;
}

Raw.prototype.sync = function(type, data, options) {
    return this.platform.http.post('/raw/'+type, data, _.merge(options||{},{token: this.platform.auth.token }) );
};

Raw.prototype.requestNewProduct = function(identity, aspects, options) {

    return this.platform.http.post('/raw/product-requests', {
        identity: identity,
        aspects: aspects,
        options: options
    }, {token: this.platform.auth.token });

};

Raw.prototype.selector = function(selectorKey, options) {
    options = options || {};

    var url = '/render/selectors/';

    if(arguments.length === 2) {
        url += selectorKey;
    }

    return this.platform.http.post(url, {
        spec: options.spec,
        options: _.omit(options, 'spec')
    }, { token: this.platform.auth.token, query: options.query });

};

module.exports = function(options) {
    options = options || {};
    return new Raw(options);
};
