var Product = require('../model/product');

module.exports = function(options) {
    options = options || {};
    return new Product(options);
};
