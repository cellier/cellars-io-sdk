'use strict';

function Winecards(options) {
    options = options || {};
    this.platform = options.platform;
}

Winecards.prototype.create = function (title) {
    return this.platform.http.post('/winecard/winecard', title, { token: this.platform.auth.token });
};

Winecards.prototype.update = function (winecardSpec) {
    return this.platform.http.put('/winecard/winecard', winecardSpec, { token: this.platform.auth.token });
};

Winecards.prototype.list = function () {
    return this.platform.http.get('/winecard/winecard/list', { token: this.platform.auth.token });
};

Winecards.prototype.activate = function (winecardSpec) {
    return this.platform.http.put('/winecard/winecard/activate', winecardSpec, { token: this.platform.auth.token });
};

Winecards.prototype.rename = function (winecardSpec) {
    return this.platform.http.post('/winecard/winecard/rename', winecardSpec, { token: this.platform.auth.token });
};

Winecards.prototype.remove = function (winecardKey) {
    return this.platform.http.delete('/winecard/winecard' + winecardKey, { token: this.platform.auth.token });
};

Winecards.prototype.rename = function (winecardSpec) {
    return this.platform.http.put('/winecard/winecard/rename', winecardSpec, { token: this.platform.auth.token });
};

module.exports = function (options) {
    options = options || {};
    return new Winecards(options);
};