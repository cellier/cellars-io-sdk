
module.exports = (function() {

    return function(provider, credentials, facebookAppId, lang, options) {
        var _this = this, logger = this.logger;
        options = options || {};

        logger.debug('Executing HTTP request POST /social/auth'+provider);
        return this.http.post('/social/auth/'+provider, { credentials: credentials, facebookAppId: facebookAppId, lang: lang }).then(function(resp) {
            if(!_this.auth) {
                logger.debug('No existing platform auth field. Initializing it');
                _this.auth = {};
            }
            if(resp) {
                logger.debug('Installing token %s as default platform auth mechanism', resp.data.token);
                _this.auth.token = resp.data.token;                
                return resp.data;
            }
            else {
                logger.error('Invalid response received from platform');
            }

        });

    };

})();
