'use strict';
var _ = require('lodash');

function Search(options) {
    options = options || {};
    this.platform = options.platform;
    this.search = true;
}

/**
 * See https://bitbucket.org/cellier/cellars_io/wiki/Product%20Matching
 *
 * @param identitySpec
 * @param options
 * @returns {*}
 */
Search.prototype.matchProduct = function(identitySpec, options) {
    options = options || {};

    _.defaults(options, {
        scope: 'catalog:stable',
        size: 10,
        offset:0,
        quality: 8,
        sort: 'name',
        projection: ':basic +media',
        threshold: 70
    });

    return this.platform.http.post('/search/catalog', {identity: identitySpec, options:options}, {token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Search(options);
};
