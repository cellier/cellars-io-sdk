'use strict';
var _ = require('lodash');

function Crm(options) {
    options = options || {};
    this.platform = options.platform;
}

Crm.prototype.createCustomer = function(spec) {
    return this.platform.http.post('/customers', spec, { token: this.platform.auth.token});
};

Crm.prototype.createProfile = function(type, username, spec) {
    return this.platform.http.post('/profiles/'+username+'/'+type, spec, { token: this.platform.auth.token});
};

Crm.prototype.updateProfile = function(type, username, spec) {
    return this.platform.http.put('/profiles/'+username+'/'+type, spec, { token: this.platform.auth.token});
};

Crm.prototype.removeProfile = function(type, username) {
    return this.platform.http.delete('/profiles/'+username+'/'+type, { token: this.platform.auth.token});
};

Crm.prototype.showProfile = function(type, username) {
    return this.platform.http.get('/profiles/'+username+'/'+type, { token: this.platform.auth.token });
};

Crm.prototype.searchProfiles = function(query, options) {
    options = options || {};
    return this.platform.http.post('/search/profiles', { query: query, options: options}, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Crm(options);
};
