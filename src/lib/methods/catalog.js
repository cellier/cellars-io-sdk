'use strict';
var _ = require('lodash');

function Catalog(options) {
    options = options || {};
    this.platform = options.platform;
}

Catalog.prototype.search = function(identity, options) {
    return this.platform.http.post('/search/catalog', {identity: identity, options: options}, { token: this.platform.auth.token });
};

//New route search in products_detail ( )
Catalog.prototype.fullsearch = function(identity, options) {
    return this.platform.http.post('/search/full/catalog', {identity: identity, options: options, apiVersion: 2}, { token: this.platform.auth.token });
};

Catalog.prototype.addAging = function(AUPID, data, applyToProduct) {
    return this.platform.http.post('/catalog/'+ AUPID + '/aging', {aging: data, applyToProduct: applyToProduct}, { token: this.platform.auth.token });
};

Catalog.prototype.improve = function(request, options) {
    return this.platform.http.post('/catalog/improvements', {request: request, options: options}, { token: this.platform.auth.token});
};

module.exports = function(options) {
    options = options || {};
    return new Catalog(options);
};
