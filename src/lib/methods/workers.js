'use strict';
var _ = require('lodash');

function Workers(options) {
    options = options || {};
    this.platform = options.platform;
}

/**
 *
 * @param workerName
 * @param options
 * @returns {*}
 */
Workers.prototype.trigger = function(workerName, options) {
    var _this = this;
    options = options || {};

    _.defaults(options, {
        query: {
            delay: 0
        }
    });

    return this.platform.http.post('/workers/'+workerName, options, { token: this.platform.auth.token});
};

/**
 * 
 * @param {type} options
 * @returns {Worker} A list of active workers
 */
Workers.prototype.list = function(options) {
    options = options || {};
    
    return this.platform.http.get('/workers', options, { token: this.platform.auth.token});
};

module.exports = function(options) {
    options = options || {};
    return new Workers(options);
};
