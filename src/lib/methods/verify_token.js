
module.exports = (function() {

    return function(token, options) {
        options = options || {};

        return this.http.get('/auth/verify/'+token, options).then(function(resp) {
            resp.data.token = token;
            return resp.data;
        });

    };

})();
