var _ = require('lodash');

function SocialEvent(options) {
    options = options || {};
    this.platform = options.platform;
}

/**
 *
 * @param keywords
 * @param options
 * @returns {*}
 */
SocialEvent.prototype.searchEvents = function(query, options) {
    options = options || {};
    return this.platform.http.post('/search/events', query, _.merge(options, {token: this.platform.auth.token }));
};

SocialEvent.prototype.getEvent = function(key) {
    return this.platform.http.get('/events/'+key, {token: this.platform.auth.token });
};

SocialEvent.prototype.listEvents = function() {
    return this.platform.http.get('/events', {token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new SocialEvent(options);
};
