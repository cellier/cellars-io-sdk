var _ = require('lodash');

function MarketValue(options) {
    options = options || {};
    this.platform = options.platform;
}

MarketValue.prototype.Apply = function(AUPID, options) {
    options = options || {};
    return this.platform.http.put('/catalog/'+ AUPID + '/marketvalue/best', {}, { token: this.platform.auth.token });
};

MarketValue.prototype.Add = function(AUPID, source, author, price, ts, currency) {
    return this.platform.http.post('/marketvalue', {AUPID:AUPID, source:source, author:author, price:price, ts:ts, currency:currency}, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new MarketValue(options);
};
