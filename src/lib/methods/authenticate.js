
module.exports = (function() {

    return function(credentials, options) {
        var _this = this, logger = this.logger;
        options = options || {};

        logger.debug('Executing HTTP request GET /auth/token');
        return this.http.get('/auth/token', {
            basic: {
                username: credentials.username,
                password: credentials.password
            }
        }).then(function(resp) {
            logger.trace(resp);
            if(!_this.auth) {
                logger.debug('No existing platform auth field. Initializing it');
                _this.auth = {};
            }
            if(resp) {
                logger.debug('Installing token %s as default platform auth mechanism', resp.data.token);
                _this.auth.token = resp.data.token;                
                return resp.data;
            }
            else {
                logger.error('Invalid response received from platform');
            }

        });

    };

})();
