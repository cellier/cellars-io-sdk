'use strict';
var _ = require('lodash');

function Analysis(options) {
    options = options || {};
    this.platform = options.platform;
}

Analysis.prototype.run = function(spec, options) {
    options = { query: options || {}, token: this.platform.auth.token};
    return this.platform.http.post('/analyze', spec, options);
};

Analysis.prototype.CalculateLargeFormat = function(AUPID, options) {
    options = options || {};
    return this.platform.http.put('/catalog/'+ AUPID + '/marketvalue/best', {}, { token: this.platform.auth.token });
};

Analysis.prototype.CalculateLargeFormat = function(AUPID, options) {
    options = { query: options || {}, token: this.platform.auth.token};
    var route = '/analysis/marketvalue/updateProductLargeFormat/' + AUPID;
    return this.platform.http.put(route, options);
};
//path: '/analysis/marketvalue/updateProductLargeFormat/{AUPID}',

module.exports = function(options) {
    options = options || {};
    return new Analysis(options);
};
