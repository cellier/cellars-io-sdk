function Corporation(options) {
    options = options || {};
    this.platform = options.platform;
}

Corporation.prototype.validateCode = function(code) {
    return this.platform.http.get('/corporation/code/' + code + '/validate', {token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Corporation(options);
};
