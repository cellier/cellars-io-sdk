var socketIOClient = require('socket.io-client');
var events = require('events');
var inherits = require('inherits');
var _ = require('lodash');
var P = require('bluebird');

function Subscriber(channelString, handler) {
    this.channelString = channelString;
    this.handler = handler;
}

Subscriber.prototype.handle = function(event) {

    if(this.channelString.indexOf('*') !== -1) {
        var channel = this.channelString.split('/')[0];
        if(event.type.indexOf(channel) !== -1) {
            this.handler(event);
        }
    }
    else if(this.channelString === event.type) {
        return this.handler(event);
    }

};

function Channel(key, api) {
    this.key = key;
    this.api = api;
    events.EventEmitter.call(this);
}
inherits(Channel, events.EventEmitter);

/*
 * Subscribe to the channel using scope token and
  * @param: options
  *     token: A valid platform token providing access to private events as per the token scope
  *     type: A string or array of event type to be notified (optional)
  *     handler: A function that will be called with the event data
  *     scope: The scope that will be used when calling the callback. default to global.
  *     persistence: (optional). Provide a persistence strategy for unreceived events. (TBD)
 */
Channel.prototype.subscribe = function(options) {
    var subscribeKey = '/' + this.key + '/';
    if(_.isString(options.type)) {
        subscribeKey += options.type;
    }
    else {
        subscribeKey += '*';
    }

    return this.api.subscribe(subscribeKey, (function(options) {
        var types = [];
        if(_.isString(options.type)) {
            types = options.type.split(',');
        }
        else {
            types = options.types;
        }

        return function(event) {
            if(types.indexOf(event.type) !== -1) {
                options.handler.call(options.scope || this, event);
            }
        };
    })(options), { token: options.token, persistence: options.persistence });
};

Channel.prototype.publish = function(event, payload) {
    this.emit(event, payload);
    return this.api.publish(event, payload);
};

function _handleEvent(event) {
    var _this = this;

    _.each(this.subscribers, function(subscriber) {

        // Provide a simple mechanism to handle replies to specific events
        subscriber.handle(event, function(reply) {
            _this.socket.emit('reply', {
                replyTo: event.id,
                data: reply
            });
        });

    });

}

function StreamAPI(options) {
    options = options || {};

    this.url = options.url;
    this.subscribers = [];
    this.connected = P.pending();

    this.socket = socketIOClient(options.url, options.socket);

    events.EventEmitter.call(this);

    // Connect all stream API event handlers
    this.socket.on('client-connected', function() {
        console.log('Stream API is CONNECTED to %s', this.url);
        this.connected.resolve();
        this.emit('connect');
    }.bind(this));

    this.socket.on('client-disconnected', function() {
        console.log('Stream API is DISCONNECTED from %s', this.url);
        this.connected = P.pending();
        this.emit('disconnect');
    }.bind(this));

    this.socket.on('event', _handleEvent.bind(this));

    // Create all secure channels
    this.catalog = new Channel('catalog', this);
    this.cellar = new Channel('cellar', this);
    this.marketplace = new Channel('marketplace', this);
    this.social = new Channel('social', this);
    this.profile = new Channel('profile', this);
    this.system = new Channel('system', this);
    this.device = new Channel('device', this);

}

inherits(StreamAPI, events.EventEmitter);

StreamAPI.prototype.getChannel = function(channelString) {
    var channel = StreamAPI[channelString];
    if(!channel) {
        channel = new Channel(channelString, this);
    }
    return channel;
};

StreamAPI.prototype.subscribe = function(channelString, handler, options) {
    options = options || {};

    // Register a listener in our socket
    this.subscribers.push(new Subscriber(channelString, handler));

    this.socket.emit('subscribe', {
        channel: channelString,
        token: options.token,
        persistence: options.persistence
    });
};

StreamAPI.prototype.publish = function(channelString, event, payload, callback) {
    this.emit(event, payload);

    // We support both callbacks and promise to get the result
    return new P(function(resolve, reject) {
        this.socket.emit(event, payload, function(err, resp) {
            if(err && callback) {
                callback(err);
                return reject(err);
            }
            else {
                if(callback) {
                    callback(null, resp);
                    return resolve(resp);
                }
            }
        });
    }.bind(this));

};

module.exports = StreamAPI;
