var request = require('request');

var MixinPromise = require('./mixin_promise');
var _ = require('lodash');

function HttpClient(baseUrl, options) {
    options = options || {};
    this.url = baseUrl;
    this.app_key = options.app_key;
    this.app_secret = options.app_secret;
    this.unsafe = options.unsafe;
    this.logger = options.logger;
    this.quality_level = options.quality_level;
    this.api_version = options.api_version;
}

HttpClient.prototype.request = function(method, path, body, options) {
    var _this = this;
    this.logger.trace('Executing HTTP request ' + method + ' ' + path);

    if(arguments.length === 3) {
        options = body;
        body = undefined;
    }
    else if(arguments.length === 2) {
        options = {};
    }

    options = options || {};

    return new MixinPromise(function(resolve, reject) {

        var headers = {};

        if(options.token) {
            _this.logger.trace('Token was provided. adding Authorization header');
            headers['authorization'] = 'Bearer ' + options.token;
            _this.logger.trace(headers['authorization']);
        }
        else if(options.basic) {
            _this.logger.trace('Basic credentials were provided. Adding Authorization header');
            var str = new Buffer(options.basic.username + ':' + options.basic.password, 'utf8').toString('base64');
            headers['authorization'] = 'Basic '+ str;
        }

        if(options.app_key || _this.app_key)
            _this.logger.trace('Configuring X-App-Key to ', options.app_key || _this.app_key);
        headers['x-app-key'] = options.app_key || _this.app_key;

        if(options.unsafe || _this.unsafe) {
            _this.logger.trace('Indicating to the platform that we are in the browser (unsafe)');
            headers['x-unsafe-auth'] = options.unsafe || _this.unsafe;
        }

        if(options.quality_level || _this.api_version) {
            headers['x-data-quality'] = options.quality_level || _this.quality_level;
        }

        if(options.api_version || _this.api_version) {
            headers['x-api-version'] = options.api_version || _this.api_version;
        }

        _this.logger.trace('Request URL is: '+ method + ' ' + _this.url + path);

        request({
            method: method,
            url: _this.url + path,
            json:body,
            headers: headers,
            verbose: true,
            withCredentials: true,
            timeout: options.timeout || 30000,
            qs: options.query
        }, function(err, resp, body) {
            if(err) {
                _this.logger.error('HTTP ERROR:', err);
                reject(err);
            }
            else {
                _this.logger.trace('Receive a valid HTTP response');
                if(resp.statusCode >= 400) {
                    _this.logger.error('Receive status code %d', resp.statusCode);
                    err = new Error('HTTP '+resp.statusCode+': '+method +' '+path);
                    err.statusCode = resp.statusCode;
                    err.error = body.error;
                    err.message = body.message;
                    err.body = body;
                    reject(err);
                }
                else if(resp.statusCode === 0) {
                    _this.logger.error('Unable to connect to platform');
                    err = new Error('Unable to connect to server');
                    err.statusCode = 0;
                    reject(err);
                }
                else if(body) {
                    _this.logger.trace('Receive a valid body');
                    var contentType = resp.getResponseHeader('content-type');
                    if(contentType.toLowerCase().indexOf('json') > -1) {
                        if (_.isString(body)) {
                            body = JSON.parse(body);
                        }
                    } else {
                        body = {
                            contentType: contentType,
                            body: body
                        };
                    }

                    if(body.data) {
                        resolve(body);
                    }
                    else {
                        _this.logger.trace('Resolving response promise. Status Code=', resp.statusCode);
                        resolve({statusCode: resp.statusCode, data: body});
                    }
                }
                else {
                    _this.logger.trace('Resolving without body. Status code = %d', resp.statusCode);
                    resolve({statusCode: resp.statusCode });
                }
            }
        });
    });
};

HttpClient.prototype.post = function(path, body, options) {
    return this.request('POST', path, body, options);
};

HttpClient.prototype.delete = function(path, options) {
    return this.request('DELETE', path, options);
};

HttpClient.prototype.get = function(path, options) {
    return this.request('GET', path, options);
};

HttpClient.prototype.put = function(path, body, options) {
    if(arguments.length === 2) {
        options = body;
        body = undefined;
    }
    return this.request('PUT', path, body, options);
};

module.exports = HttpClient;
