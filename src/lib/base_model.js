var http = require('./http-client');
var P = require('bluebird').noConflict();
var _ = require('lodash');

function Model(endpoint, options) {
    options = options || {};
    this.platform = options.platform;
    this.endpoint = endpoint || options.endpoint;
    this.concurrency = options.concurrency || 5;
    this.prefix = options.prefix || '';
}

Model.prototype.show = function(idOrQuery, options) {
    options = options || {};
    if(_.isPlainObject(idOrQuery)) {
        return this.platform.http.get(this.prefix + '/'+this.endpoint, _.merge(options || {}, {token: this.platform.auth.token, query: { q: JSON.stringify(idOrQuery) }}));
    }
    else {
        return this.platform.http.get(this.prefix + '/'+this.endpoint + '/' + idOrQuery, _.merge(options, {token: this.platform.auth.token }));
    }
};

Model.prototype.create = function(doc, options) {
    options = options || {};
    return this.platform.http.post(this.prefix + '/'+this.endpoint, doc, _.merge(options, { token: this.platform.auth.token }));
};

Model.prototype.list = function(options) {
    options = options || {};
    return this.platform.http.get(this.prefix + '/'+this.endpoint, options, _.merge(options, { token: this.platform.auth.token }));
};

Model.prototype.remove = function(idOrQuery, options) {
    if(_.isPlainObject(idOrQuery)) {
        return this.platform.http.delete(this.prefix + '/'+this.endpoint, _.merge(options || {}, {token: this.platform.auth.token, query: { q: JSON.stringify(idOrQuery) }}));
    }
    else {
        return this.platform.http.delete(this.prefix + '/'+this.endpoint + '/' + idOrQuery, _.merge(options, {token: this.platform.auth.token}));
    }
};

Model.prototype.upsert = function(document, options) {
    var _this = this;
    options = options || {};

    if(options.batch) {
        return P.all(P.map(document, function(doc) {
            return _this.platform.http.post(this.prefix + '/'+_this.endpoint+ '/' + doc.key, doc, _.merge(options, { token: _this.platform.auth.token }));
        }), { concurrency: options.concurrency || _this.concurrency });
    }
    else {
        return this.platform.http.post(this.prefix + '/'+_this.endpoint, document, _.merge(options, { token: _this.platform.auth.token }));
    }

};

Model.prototype.update = function(document, options) {
    var _this = this;
    options = options || {};

    if(options.batch) {
        return P.all(P.map(document, function(doc) {
            return _this.platform.http.put(this.prefix + '/'+_this.endpoint+ '/' + doc.key, doc, _.merge(options, { token: _this.platform.auth.token }));
        }), { concurrency: options.concurrency || _this.concurrency });
    }
    else {
        return this.platform.http.post(this.prefix + '/'+_this.endpoint + '/' + document.key, document, _.merge(options, { token: _this.platform.auth.token }));
    }

};

module.exports = Model;
