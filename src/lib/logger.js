   
function Logger(level) {
    this.level = level;
}

Logger.prototype.trace = function() {
    if(this.level > 9) {
        console.log.apply(console, arguments);
    }
};

Logger.prototype.debug = function() {
    if(this.level > 7) {
        console.log.apply(console, arguments);
    }
};

Logger.prototype.info = function() {
    if(this.level > 5) {
        console.log.apply(console, arguments);
    }
};

Logger.prototype.warn = function() {
    if(this.level > 3) {
        console.log.apply(console, arguments);
    }
};

Logger.prototype.error = function() {
    if(this.level > 1) {
        console.log.apply(console, arguments);
    }
};

Logger.prototype.fatal = function() {
    console.log.apply(console, arguments);
};

module.exports = Logger;
