
module.exports = {
    // Not activated
    "utest1": {
        "username": "utest1",
        "password": "utest!",
        "email": "utest1@cellars.io"
    },
    // Activated - valid
    "utest2": {
        "username": "utest2",
        "password": "utest@",
        "email": "utest2@cellars.io"
    }
};
