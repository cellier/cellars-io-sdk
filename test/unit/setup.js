// Export modules to global scope as necessary (only for testing)
var SDK = require('../../src');
var _ = require('lodash');

module.exports = {
    platformVersion: '1.3.1',
    platformName: "cellars-io",
    newInstance: function(options) {
        return new SDK(options);
    },
    credentials: require('../data/credentials.js')
};
