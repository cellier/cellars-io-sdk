var Tools = require('../setup');
var expect = require('chai').expect;
var nock = require('nock');

describe('get-current-user', function() {
    var engine;

    before(function() {
        engine = Tools.newInstance();
    });

    it('should return a valid user profile', function(done) {
        nock('https://api.cellars.io').get('/user').reply(200, {username: 'UserTest1'});
        engine.getCurrentUser().then(function(result) {
            expect(result.statusCode).to.equal(200);
            expect(result.data.username).to.equal("UserTest1");
            done();
        }).catch(done);
    });
});
