var Tools = require('../setup');
var expect = require('chai').expect;
var nock = require('nock');

describe('get-current-user', function() {
    var engine;

    before(function () {
        engine = Tools.newInstance();
    });

    it('should return a valid system status', function(done) {
        this.timeout(2000);

        nock('https://api.cellars.io')
            .get('/status')
            .reply(200, {
                platform: Tools.platformName,
                version: Tools.platformVersion,
                systemInfo: [ ]
            });

        engine.getSystemStatus().then(function(result) {
            expect(result.statusCode).to.equal(200);
            done();
        }).catch(function(err) {
            done(err);
        });
    });

});
